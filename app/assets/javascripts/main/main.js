document.addEventListener("turbolinks:load", function () {
    // BEGIN

    $(".titleSpan").fitText(1.9, {minFontSize: '5px', maxFontSize: '25px'});
    $(".previewTitle").fitText(2.2, {minFontSize: '5px', maxFontSize: '30px'});
    $(".relatedTitle").fitText(1.8, {minFontSize: '5px', maxFontSize: '16px'});
    $(".navCatalog h1").fitText(2.1, {minFontSize: '5px', maxFontSize: '26px'});

    $('.menuButton').click(function () {
        $('.catMenuHide').hide(500);
        $('.navVaries').slideToggle();
    });

    $('.menuButton2').click(function () {
        if($(window).width() <=480) {
            $('.navVaries').hide(500);
        };
        $('.catMenuHide').slideToggle();
    });
// END

});

// BEGIN commint
document.addEventListener("turbolinks:load", function () {
    $('.addForm').on('click', function () {
        var idCommint = $(this).attr('id');
        if (!$('#addForm_' + idCommint).length) {
            $('#commId_' + idCommint).after('<ul class="CommentsInside" id="addForm_' + idCommint + '"></ul>');
        }
        ;
        if ($('#addForm_' + idCommint + ' > .addCommintForm').is(':visible')) {
            $('#addForm_' + idCommint + ' > .addCommintForm').remove();
        } else {
            $('#addForm_' + idCommint).prepend(
                '<li class="addCommintForm">\
                   <br><form class="new_twocomment" id="new_twocomment" action="/twocomments" accept-charset="UTF-8" data-remote="true" method="post">\
                       <input name="utf8" value="✓" type="hidden">\
                       <div class="inputWrap"><input class="commentName" type="text" name="twocomment[name]" id="twocomment_name" placeholder="Ваше имя" required><span class="areaSpan"></span></div>\
                       <div class="inputWrap"><textarea name="twocomment[body]"  placeholder="Ваше сообщение" required rows="2" maxlength="2000" id="twocomment_body"></textarea><span class="areaSpan"></span></div>\
                       <input value="' + idCommint + '" name="twocomment[comment_id]" id="twocomment_comment_id" type="hidden">\
                       <div class="WrapCommitSubmit">\
                       <button name="button" type="submit" class="commitsSubmit">Отправить</button>\
                       </div>\
                   </form>\
               </li>'
            );
        }
    });
});


// END commint


$(document).ready(function () {
    $("#mainPostImage").fancybox({
        parent: "body",
        helpers: {
            title: {
                type: 'float'
            }
        }
    });

    $(".postSlide").fancybox({
        parent: "body",
        helpers: {
            title: {
                type: 'float'
            }
        }
    });
});

// BEGIN ajax add city to company
$(document).ready(function () {
    $(document).on("change", "#company_city", function () {
        $.ajax({
            url: '/companies/form_render',
            type: 'POST',
            data: $(this).serialize(),
            success: function (result) {
                $('#city').html(result);
                console.log(result);
            }
        });
    });
});
//    END ajax add city to company

document.addEventListener("turbolinks:load", function () {

    $('.searchInput').autocomplete({
        minLength: 3,
        delay: 600,
        source: '/search/autocomplete.json'
    });
    $('.ui-helper-hidden-accessible').hide(); //hide result message in div classes ui-helper-hidden-accessible
});

document.addEventListener("turbolinks:load", function () {
    $('body').append('<div id="topNubex"><img src="/up.png"></div>');
    $('#topNubex').hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('#topNubex').show();
        } else {
            $('#topNubex').fadeOut();
        }
    });
    $('#topNubex').click(function () {
        $('body,html').animate({scrollTop: 0}, 700);
    });
});


// BEGIN nav right style fixed
document.addEventListener("turbolinks:load", function () {
    $(window).on('scroll', function () {
        if ($(window).width() >= 992) {
            var height_nav = $('#mainHeaderMenu').height();
            var height_navCat = $('.navCatalog').height();
            var resultH = height_nav + height_navCat;
            resultH += 20;
            if ($(this).scrollTop() > resultH) {
                $('.navRightBlock').css({"position": "fixed", "margin-left": "690px", "top": "0"});

            } else {
                $('.navRightBlock').css({"position": "relative", "margin-left": "0"});
            }
            var dist = $('.footerWall').offset().top - $('.navRightBlock').offset().top - $('.navRightBlock').outerHeight();
            if (dist < 10) {
                $('.CategoryRight').hide();
            }
            if (dist > 500) {
                $('.CategoryRight').show();
            }
        }
        $(window).resize(function () {
            windowWidth = $(this).width();
            if (windowWidth < 992) {
                $('.navRightBlock').css({"position": "relative", "margin-left": "0"});
            }
        });
    });
});
// END nav right style fixed

function sendAnimate(attr) {
    $(attr).html('<img src="/sender.gif">');
}

// BEGIN ajax clickLike post
$(document).ready(function () {
    $(document).on("click", ".clickLike", function () {
        var id_post = $(this).attr('id');
        $('#likePost_'+id_post+' span').remove();
        $('#likePost_'+id_post).prepend("<img src='/load-like15.gif'>");
        $.ajax({
            url: '/posts/like_post',
            type: 'POST',
            data: "id="+id_post,
            success: function (result) {
                console.log(result);
            }
        });
    });
});
//    END ajax ajax clickLike post


