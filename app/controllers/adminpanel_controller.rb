class AdminpanelController < ApplicationController
  before_action :authenticate_rootadmin!
  skip_before_action :cat
  layout 'adminpanel'

  def index
    @post = Post.count
    @category = Category.count
    @twocategory = Twocategory.count
    @comment = Comment.count
    @commentnew = Comment.where(moderation: 0).count
    @twocomment = Twocomment.count
    @twocommentnew = Twocomment.where(moderation: 0).count
    @catalog = Catalog.count
    @company = Company.count
    @companynew = Company.where(moderation: 0).count
    @feed = Feed.count
    @feednew = Feed.where(moderation: 0).count
  end

  def categories
    @category = Category.paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
  end

  def twocategories
    @twocategory = Twocategory.paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
  end

  def posts
    @post = Post.paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
  end

  def comments
    @comment = Comment.paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
    @sort = params[:moderation]
    if @sort == 'all'
      @comment
    elsif @sort
      @comment = Comment.where(moderation: params[:moderation]).paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
    end
  end

  def twocomments
    @twocomment = Twocomment.paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
    @sort = params[:moderation]
    if @sort == 'all'
      @twocomment
    elsif @sort
      @twocomment = Twocomment.where(moderation: params[:moderation]).paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
    end
  end

  def catalogs
    @catalogs = Catalog.paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
  end

  def companies
    @companies = Company.paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
  end

  def feedback
    @feed = Feed.paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
    @sort = params[:moderation]
    if @sort == 'all'
      @feed
    elsif @sort
      @feed = Feed.where(moderation: params[:moderation]).paginate(:page => params[:page], :per_page => 20).order(created_at: :desc)
    end
  end


end
