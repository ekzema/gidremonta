class ApplicationController < ActionController::Base
  before_action :cat, :setting_site, :metateg
  # protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  protect_from_forgery with: :exception

  def cat
    @categories = Category.all
  end

  def metateg
    @title = 'Статьи про ремонт и строительство на портале: ' + @domen
    @description = 'Блогремонта - это сайт для тех кто делает ремонт квартиры своими руками, а также интересные и креативные решения в строительстве и дизайне'
    @keywords = 'статьи, ремонт квартиры, строительсво, своими руками, инструмент, каталог фирм, каталог компатний, стройка, самоделки'
  end

  def setting_site
    @domen = 'blogremonta.com'
  end

  def not_found
    render_404 if Rails.env == 'production'
  end

  def render_404
    render :file => "#{Rails.root}/public/404", :layout => false, :status => 404
  end

  def success_response(success = true, message = nil, status = 200)
    message ||= 'ok'
    render json: {:success => success, :message => message}, :status => status
  end
end
