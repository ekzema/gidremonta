class CatalogsController < ApplicationController
  before_action :authenticate_rootadmin!, except: [:show, :index]
  before_action :set_catalog, only: [:edit, :update, :destroy]
  before_action :set_trarslit_catalog, only: [:show]
  layout 'adminpanel', only: [:new, :edit]

  # GET /catalogs
  # GET /catalogs.json
  def index
    @catalogs = Catalog.all
    @title = 'Каталог строительнх компаний и фирм'
    @description = 'В этом разделе представлены компании, фирмы и подрядчики по ремонту квартир и строительству'
    @keywords = @catalogs.pluck(:name).join(', ')
    @companies = Company.where(moderation: 1).paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
  end

  # GET /catalogs/1
  # GET /catalogs/1.json
  def show
    @catalogs = Catalog.all
    @title = 'Строительный каталог: ' + @catalog.name
    @description = 'В этом разделе представлены компании, фирмы и подрядчики по ремонту квартир и строительству'
    @keywords = @catalogs.pluck(:name).join(', ')
    @companies = @catalog.companies.where(moderation: 1).paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
  end

  # GET /catalogs/new
  def new
    @catalog = Catalog.new
  end

  # GET /catalogs/1/edit
  def edit
  end

  # POST /catalogs
  # POST /catalogs.json
  def create
    @catalog = Catalog.new(catalog_params)

    respond_to do |format|
      if @catalog.save
        format.html { redirect_to adminpanel_catalogs_path, notice: "Каталог #{@catalog.name} успешно создан"  }
        format.json { render :show, status: :created, location: adminpanel_catalogs_path }
      else
        format.html { render :new }
        format.json { render json: @catalog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /catalogs/1
  # PATCH/PUT /catalogs/1.json
  def update
    respond_to do |format|
      if @catalog.update(catalog_params)
        format.html { redirect_to adminpanel_catalogs_path, notice: "Каталог #{@catalog.name} успешно обновлён"  }
        format.json { render :show, status: :ok, location: adminpanel_catalogs_path }
      else
        format.html { render :edit }
        format.json { render json: @catalog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /catalogs/1
  # DELETE /catalogs/1.json
  def destroy
    success_response if @catalog.destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_catalog
    @catalog = Catalog.find(params[:id]) or not_found
  end

  def set_trarslit_catalog
    @catalog = Catalog.find_by_translit_url(params[:name]) or not_found
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def catalog_params
    params.require(:catalog).permit(:name, :translit_url, :meta_desc, :meta_key)
  end
end
