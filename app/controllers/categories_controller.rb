class CategoriesController < ApplicationController
  before_action :authenticate_rootadmin!, except: [:show]
  before_action :set_category, only: [:edit, :update, :destroy]
  before_action :set_trarslit_category, only: [:show]
  layout 'adminpanel', only: [:new, :edit]
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @title = 'Статьи на тему: ' + @category.name
    @description = @category.meta_desc if @category.meta_desc.present?
    @keywords = @category.meta_key if @category.meta_key.present?
    @categories = Category.all
    @twocategories = @category.twocategories.ids
    @posts = Post.where(twocategory_id: @twocategories, visible: 1).paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to adminpanel_categories_path, notice: "Категория #{@category.name} успешно создана"  }
        format.json { render :show, status: :created, location: adminpanel_categories_path }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to adminpanel_categories_path, notice: "Категория #{@category.name} успешно обновлена" }
        format.json { render :show, status: :ok, location: adminpanel_categories_path }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    success_response if @category.destroy
  end

  def delete_attachment
    category = Category.find(params[:id])
    category.image = nil
    category.save
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id]) or not_found
    end

    def set_trarslit_category
      @category = Category.find_by_translit_url(params[:name]) or not_found
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :meta_title, :meta_desc, :meta_key, :image, :translit_url)
    end
end
