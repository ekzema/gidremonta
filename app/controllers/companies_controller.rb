class CompaniesController < ApplicationController
  before_action :authenticate_rootadmin!, except: [:show, :new, :create, :form_render]
  before_action :set_company, only: [:edit, :update, :destroy]
  before_action :set_regular, only: [:show]
  layout 'adminpanel', only: [:edit]
  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    cat_name = []
    @title = 'Компания: ' + @company.name
    @description ="#{@company.name}, #{@company.state}, полная информация о компании"
    @company.company_skills.each{|skill| cat_name << skill.catalog.name}
    @keywords = cat_name.join(', ')
    @catalogs = Catalog.all
  end

  # GET /companies/new
  def new
    @title = 'Добавить строительную компанию или фирму в каталог'
    @company = Company.new
    @company.company_skills.build
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  # POST /companies.json
  def create

    @company = Company.new(company_params)
    respond_to do |format|
      if @company.save
        WlcomeMailer.new_company.deliver
        format.js
      else
        format.js
      end
    end
  end


  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_rootadmin_params)
        format.html { redirect_to adminpanel_companies_path, notice: 'Company was successfully updated.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end


  def form_render
    if params[:company][:state]
      if params[:company][:state].blank?
        render text: ''
      else
        @state = params[:company][:state]
        render :partial => 'city', locals: { state: @state }
      end
    end

  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    success_response if @company.destroy
  end

  def delete_attachment
    company = Company.find(params[:id])
    company.image = nil
    company.save
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id]) or not_found
    end

  def set_regular
    @company = Company.find(params[:id]) or not_found
  end
    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name, :phone, :email, :site, :description, :state, :city, :adress, :image,
                                      company_skills_attributes: [:id, :_destroy, :catalog_id]
      )
    end

  def company_rootadmin_params
    params.require(:company).permit(:name, :phone, :email, :site, :description, :moderation, :state, :city, :adress, :image,
                                    company_skills_attributes: [:id, :_destroy, :catalog_id]
    )
  end
end
