class CompanySkillsController < ApplicationController

  def create
    company = Company.find(params[:company_id])
    company_skill = company.company_skills.create(company_skill_params)
  end

   private
  def company_skill_params
    params.require(:company_skill).permit([:catalog_id])
  end

end
