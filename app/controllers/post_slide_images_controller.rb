class PostSlideImagesController < ApplicationController
  before_action :authenticate_rootadmin!
  def create
    post = Post.find(params[:post_id])
    post_slide_image = post.post_slide_images.create(post_slide_image_params)
  end

  private
  def post_slide_image_params
    params.require(:post_slide_image).permit([:image])
  end

end
