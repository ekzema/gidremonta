class PostsController < ApplicationController
  before_action :authenticate_rootadmin!, except: [:show, :like_post]
  skip_before_action :verify_authenticity_token, only: [:update]
  before_action :set_post, only: [:like_post, :edit, :update, :destroy]
  before_action :set_trarslit_post, only: [:show]
  before_action :cat, only: [:show]
  layout 'adminpanel', only: [:new, :edit, :update, :create]
  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    redirect_back(fallback_location: root_path, notice: 'Страница временно недоступна') if @post.visible == 0
    @title = @post.name + ' | Blogremonta'
    @description = @post.meta_desc
    @keywords = @post.meta_key
    @category = @post.twocategory.category
    @twocategory = @post.twocategory
    @twocategories = Twocategory.where(category_id: @category.id)
    if browser.known? and cookies["view_post_#{@post.id}"].nil?
      cookies["view_post_#{@post.id}"] = true
      view_count = @post.view_count + 1
      @post.update(view_count: view_count)
    end
    @categories = Category.all
    array_product = @category.twocategories.map { |i| i.posts.ids.delete_if { |i| i == @post.id } }.flatten.shuffle.take(2)
    @post_random = Post.where(id: array_product)

  end

  def like_post
    unless cookies["like_post_#{@post.id}"].present?
      cookies["like_post_#{@post.id}"] = {:value => true, :expires => 20.years.from_now.utc}
      like_count = @post.like_count + 1
      @post.update(like_count: like_count)
      respond_to do |format|
        format.js
      end

    end
  end

  def form_render
    @twocategory = Twocategory.where(category_id: params[:category_id])
    if params[:category_id]
      if params[:category_id].blank?
        render text: ''
      else
        render :partial => 'twocategory', locals: {twocategory: @twocategory}
      end
    end

  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    @category = @post.twocategory.category.id
    @twocategories = Twocategory.where(category_id: @post.twocategory.category)
  end

  # POST /posts
  # POST /posts.json

  # @box = Box.find(params[:box_id])
  # @filtr = @box.filtrs.create(filtr_params)

  def create
    @twocategories = Twocategory.all
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        if params[:post][:image].present?
          return render :crop
        end
        @post.post_slide_images.build
        return redirect_to edit_post_path(@post) if params[:back]
        format.html { redirect_to adminpanel_posts_path, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: adminpanel_posts_path }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      $postimg = params[:post][:image].present?
      if @post.update(post_params)
        if $postimg
          return render :crop
        end
        @post.post_slide_images.build
        return redirect_to edit_post_path(@post) if params[:back]
        format.html { redirect_to adminpanel_posts_path, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: adminpanel_posts_path }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json

  def destroy
    success_response if @post.destroy
  end

  def delete_attachment
    @post = Post.find(params[:id])
    @post.image = nil
    @post.save
    respond_to do |format|
      format.js
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id]) or not_found
  end

  def set_trarslit_post
    @post = Post.find_by_translit_url(params[:name]) or not_found
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def post_params
    params.require(:post).permit(:name, :description, :desc_min, :body, :meta_title, :meta_desc, :meta_key, :image, :view_count, :visible,  :like_count, :twocategory_id, :translit_url, :image_original_w, :image_original_h, :image_box_w, :image_aspect, :image_crop_x, :image_crop_y, :image_crop_w, :image_crop_h, post_slide_images_attributes: [:id, :_destroy, :image]
    )
  end
end
