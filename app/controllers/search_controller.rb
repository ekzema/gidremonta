class SearchController < ApplicationController

  def result
    @title = 'Результат поиска: ' + params[:q] + ' | ' + @domen
    if params[:q].blank?
      redirect_to :back, notice: 'Введите текст для поиска!'
    else
      @posts = Post.order(:name).where("name LIKE ?", "%#{params[:q]}%")
    end
  end

  def autocomplete
    @post = Post.order(:name).where("name LIKE ?", "%#{params[:term]}%").limit(10)
    render json: @post.map{|i| i.name}
  end

end
