class TwocategoriesController < ApplicationController
  before_action :authenticate_rootadmin!, except: [:show]
  before_action :set_twocategory, only: [:edit, :update, :destroy]
  before_action :set_trarslit_twocategory, only: [:show]
  layout 'adminpanel', only: [:new, :edit]

  # GET /twocategories
  # GET /twocategories.json
  def index
    @twocategories = Twocategory.all
  end

  # GET /twocategories/1
  # GET /twocategories/1.json
  def show
    @title = 'Статьи на тему: ' + @twocategory.name
    @categories = Category.all
    @twocategories = Twocategory.where(category_id: @twocategory.category.id)
    @posts = @twocategory.posts.where(visible: 1).paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
  end

  # GET /twocategories/new
  def new
    @twocategory = Twocategory.new
  end

  # GET /twocategories/1/edit
  def edit
  end

  # POST /twocategories
  # POST /twocategories.json
  def create
    @twocategory = Twocategory.new(twocategory_params)

    respond_to do |format|
      if @twocategory.save
        format.html { redirect_to adminpanel_twocategories_path, notice: "Подкатегория #{@twocategory.name} успешно создана" }
        format.json { render :show, status: :created, location: @twocategory }
      else
        format.html { render :new }
        format.json { render json: @twocategory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /twocategories/1
  # PATCH/PUT /twocategories/1.json
  def update
    respond_to do |format|
      if @twocategory.update(twocategory_params)
        format.html { redirect_to adminpanel_twocategories_path, notice: "Подкатегория #{@twocategory.name} успешно создана" }
        format.json { render :show, status: :ok, location: adminpanel_twocategories_path }
      else
        format.html { render :edit }
        format.json { render json: @twocategory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /twocategories/1
  # DELETE /twocategories/1.json
  def destroy
    success_response if @twocategory.destroy
  end

  def delete_attachment
    twocategory = Twocategory.find(params[:id])
    twocategory.image = nil
    twocategory.save
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_twocategory
      @twocategory = Twocategory.find(params[:id]) or not_found
    end

  def set_trarslit_twocategory
    @category = Category.find_by_translit_url(params[:category_name]) or not_found
    @twocategory = Twocategory.where(category_id: @category, translit_url: params[:name]).first
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def twocategory_params
      params.require(:twocategory).permit(:name, :meta_title, :meta_desc, :meta_key, :category_id, :image, :translit_url)
    end
end
