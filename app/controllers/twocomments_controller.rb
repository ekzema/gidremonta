class TwocommentsController < ApplicationController
  before_action :authenticate_rootadmin!, except: [:create]
  before_action :set_twocomment, only: [:show, :edit, :update, :destroy]
  layout 'adminpanel', only: [:edit]

  # GET /twocomments
  # GET /twocomments.json
  def index
    @twocomments = Twocomment.all
  end

  # GET /twocomments/1
  # GET /twocomments/1.json
  def show
  end

  # GET /twocomments/new
  def new
    @twocomment = Twocomment.new
  end

  # GET /twocomments/1/edit
  def edit
  end

  # POST /twocomments
  # POST /twocomments.json
  def create
  params[:twocomment][:user_ip] = request.ip
  @comment = Comment.find(params[:twocomment][:comment_id])
  @twocomment = @comment.twocomments.new(twocomment_params)

  respond_to do |format|
    if @twocomment.save
      format.js
    else
      format.js
    end
  end
end

  # PATCH/PUT /twocomments/1
  # PATCH/PUT /twocomments/1.json
  def update

    respond_to do |format|
      if @twocomment.update(admin_twocomment_params)
        format.html { redirect_to adminpanel_twocomments_path, notice: 'Комментарий2 успешно обновлён' }
        format.json { render :show, status: :ok, location: adminpanel_twocomments_path }
      else
        format.html { render :edit }
        format.json { render json: @twocomment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /twocomments/1
  # DELETE /twocomments/1.json
  def destroy
    success_response if @twocomment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_twocomment
      @twocomment = Twocomment.find(params[:id]) or not_found
    end

  def admin_twocomment_params
    params.require(:twocomment).permit(:name, :body, :post_id, :moderation)
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def twocomment_params
      params.require(:twocomment).permit(:name, :body, :comment_id, :user_ip)
    end
end
