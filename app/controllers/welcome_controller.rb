class WelcomeController < ApplicationController
  before_action :set_welcome
  skip_before_action :cat

  def index
    @posts = Post.where(visible: 1).paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
  end

  def feedback
    @title = 'Обратная связь | ' + @domen
    @feed = Feed.new
    @categorias = Category.all
  end

  def reklama
    @title = 'Реклама на сайте | ' + @domen
    @feed = Feed.new
    @categorias = Category.all
  end

  def about
    @title = 'О проекте - Блогремонта | ' + @domen
    @categorias = Category.all
  end

  def sitemap
    @categories = Category.all
  end


  private

  def set_welcome
    @categories = Category.all
  end


end
