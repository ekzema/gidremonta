module ApplicationHelper

  def count_comments_post post_id
    @comments = post_id.comments.where(moderation: 1)
    @count_comment = post_id.comments.where(moderation: 1).count
    @count_twocomments = Twocomment.where(comment_id: @comments.ids, moderation: 1)
    @count_comment = @count_comment + @count_twocomments.count
  end

  def count_posts category
    i = 0
    category.twocategories.each {|twocategory| i += twocategory.posts.count}
    return i
  end

end
