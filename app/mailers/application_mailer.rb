class ApplicationMailer < ActionMailer::Base
  default from: 'info@blogremonta.com'
  layout 'mailer'
end
