class WlcomeMailer < ApplicationMailer

  def new_message
    mail(to: 'ekzemadev@gmail.com', subject: 'Новое сообщение')
  end

  def new_company
    mail(to: 'ekzemadev@gmail.com', subject: 'Добавлена новая фирма')
  end

end
