class Comment < ApplicationRecord
  belongs_to :post
  has_many :twocomments, :dependent => :destroy
  validates :name, :presence => {message: 'укажите своё имя'}
  validates :body, :presence => {message: 'напишите комментарий'}
end
