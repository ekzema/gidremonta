class Company < ApplicationRecord
  validates :name, :presence => {message: 'укажите своё имя'}
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: 'указан не корректный Email'
  validates :state, :presence => {message: 'укажите свою страну'}
  validates :city, :presence => {message: 'укажите свою область'}
  validates_inclusion_of :state, :in => Catalog::STATE_TYPES + [''], :message => 'Выбран не верный формат страны'
  validates :description, :presence => {message: 'укажите краткое описание компании'}
  validates :company_skills, :presence => {message: 'выберите каталог'}

  has_attached_file :image, :styles => {:original => "16x" }, :default_url => "no-logo-company.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  has_many :company_skills, :dependent => :destroy
  accepts_nested_attributes_for :company_skills,
                                :allow_destroy => true,
                                :reject_if => :all_blank

  def to_param
    "#{id}-#{name.parameterize}"
  end

end
