class Feed < ApplicationRecord

  validates :name, :presence => {message: 'укажите своё имя'}
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: 'указан не корректный Email'
  validates :title, :presence => {message: 'укажите тему сообщения'}
  validates :description, :presence => {message: 'напишите описание'}


end
