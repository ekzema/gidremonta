class Post < ApplicationRecord
  before_create :rename_image
  before_update :rename_image_update

  has_many :comments, :dependent => :destroy
  belongs_to :twocategory
  has_many :post_slide_images, :dependent => :destroy
  accepts_nested_attributes_for :post_slide_images,
                                :allow_destroy => true,
                                :reject_if => :all_blank

  validates :translit_url, :uniqueness => true

  has_attached_file :image, :styles => { :admin => "X100", :thumb => "X60", :preloadMain => "680X"}, :default_url => "noimage.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  crop_attached_file :image, :aspect => "68:35"

  def rename_image
    if self.image? and self.translit_url.present?
      extension = File.extname(image_file_name)
      self.image.instance_write :file_name, "#{self.translit_url.mb_chars.downcase.to_s}#{extension}"
    end

  end

  def rename_image_update
    if $postimg
      extension = File.extname(image_file_name)
      self.image.instance_write :file_name, "#{self.translit_url.mb_chars.downcase.to_s}#{extension}"
    end
  end

  def get_id slide_images
    slide_images.object_name.split('[')[-1].first(-1)
  end

  # def transtlit_name(name)
  #   name.gsub(/[а-ё\s]/,
  #             'а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e', 'ж'=>'zh', 'з'=>'z',
  #             'и'=>'i', 'й'=>'y', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 'п'=>'p',
  #             'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'ы'=>'i', 'э'=>'e', 'ё'=>'yo',
  #             'х'=>'h', 'ц'=>'ts', 'ч'=>'ch', 'ш'=>'sh', 'щ'=>'shch', 'ъ'=>'', 'ь'=>'', 'ю'=>'yu', 'я'=>'ya',
  #             ' ' => '-' )
  # end


  # transtlit_name all Characters
  # def transtlit_name(name)
  #   name.gsub(/[а-ё+','+'?'+'!'+\.\s]/,
  #             'а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e', 'ж'=>'zh', 'з'=>'z',
  #             'и'=>'i', 'й'=>'y', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 'п'=>'p',
  #             'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'ы'=>'i', 'э'=>'e', 'ё'=>'yo',
  #             'х'=>'h', 'ц'=>'ts', 'ч'=>'ch', 'ш'=>'sh', 'щ'=>'shch', 'ъ'=>'', 'ь'=>'', 'ю'=>'yu', 'я'=>'ya',
  #             ' ' => '-', ',' => '-', '?' => '', '!' => '', '.' => '')
  # end

end
