class PostSlideImage < ApplicationRecord

  before_create :rename_image
  before_update :rename_image

  belongs_to :post

  has_attached_file :image, :default_url => "noimage.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def rename_image
    if self.image? and self.post.translit_url.present?
      extension = File.extname(image_file_name)
      self.image.instance_write :file_name, "#{self.post.translit_url.mb_chars.downcase.to_s}-#{PostSlideImage.maximum(:id).to_i + 1}#{extension}"
    end
  end

  # def transtlit_name(name)
  #   name.gsub(/[а-ё\s]/,
  #             'а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e', 'ж'=>'zh', 'з'=>'z',
  #             'и'=>'i', 'й'=>'y', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 'п'=>'p',
  #             'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'ы'=>'i', 'э'=>'e', 'ё'=>'yo',
  #             'х'=>'h', 'ц'=>'ts', 'ч'=>'ch', 'ш'=>'sh', 'щ'=>'shch', 'ъ'=>'', 'ь'=>'', 'ю'=>'yu', 'я'=>'ya',
  #             ' ' => '-' )
  # end

end
