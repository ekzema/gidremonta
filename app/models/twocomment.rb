class Twocomment < ApplicationRecord
  belongs_to :comment
  validates :name, :presence => {message: 'укажите своё имя'}
  validates :body, :presence => {message: 'напишите комментарий'}
end
