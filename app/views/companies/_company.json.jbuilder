json.extract! company, :id, :name, :phone, :email, :site, :description, :state, :city, :moderation, :created_at, :updated_at
json.url company_url(company, format: :json)