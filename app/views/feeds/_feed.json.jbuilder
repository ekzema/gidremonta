json.extract! feed, :id, :name, :email, :title, :description, :moderaration, :created_at, :updated_at
json.url feed_url(feed, format: :json)
