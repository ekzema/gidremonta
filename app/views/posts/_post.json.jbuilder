json.extract! post, :id, :name, :description, :desc_min, :body, :meta_title, :meta_desc, :meta_key, :created_at, :updated_at
json.url post_url(post, format: :json)