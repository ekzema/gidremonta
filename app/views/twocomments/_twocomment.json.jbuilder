json.extract! twocomment, :id, :name, :body, :moderation, :comment_id, :created_at, :updated_at
json.url twocomment_url(twocomment, format: :json)