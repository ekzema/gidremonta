Rails.application.routes.draw do

  devise_for :rootadmins, controllers: {
      sessions: "rootadmins/sessions",
      registrations: "rootadmins/registrations",
      unlocks: "rootadmins/unlocks",
      passwords: "rootadmins/passwords",
      omniauth: "rootadmins/omniauth",
      confirmations: "rootadmins/confirmations"
  } do
  end

  devise_scope :rootadmin do
    get "/rootadmins" => "rootadmins/sessions#new"
  end


  resources :feeds, except: [:index, :show]
  root 'welcome#index'

  namespace :search do
    get 'autocomplete'
    get 'result'
  end

  resources :catalogs
  get 'catalog/:name', to: 'catalogs#show', as: 'showcatalog'

  resources :companies do
    collection do
      delete 'delete_attachment'
    end
  end
  post 'companies/form_render'


  get 'adminpanel', to: 'adminpanel#index'
  get 'feedback', to: 'welcome#feedback', as: 'feedback'
  get 'reklama', to: 'welcome#reklama', as: 'reklama'
  get 'about', to: 'welcome#about', as: 'about'
  get 'sitemap', to: 'welcome#sitemap', as: 'sitemap'

  namespace :adminpanel do
    get 'categories'
    get 'twocategories'
    get 'posts'
    get 'comments'
    get 'twocomments'
    get 'catalogs'
    get 'companies'
    get 'feedback'
  end

  resources :categories, except: [:index] do
    collection do
      delete 'delete_attachment'
    end
  end
  get 'category/:name', to: 'categories#show', as: 'showcategory'

  resources :twocategories, except: [:index] do
    collection do
      delete 'delete_attachment'
    end
  end
  get 'category/:category_name/:name', to: 'twocategories#show', as: 'showtwocategory'

  resources :posts, except: [:index] do
    collection do
      post 'like_post'
      delete 'delete_attachment'
    end
  end
  get 'post/:name', to: 'posts#show', as: 'showpost'
  post 'posts/form_render'

  resources :comments, only: [:edit, :create, :update, :destroy]
  resources :twocomments, only: [:edit, :create, :update, :destroy]
end
