# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://blogremonta.com"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  add feedback_path, :changefreq => 'monthly', :lastmod => '2017-11-03'
  add reklama_path, :changefreq => 'monthly', :lastmod => '2017-11-03'
  add about_path, :changefreq => 'monthly', :lastmod => '2017-11-03'
  add sitemap_path, :changefreq => 'daily'
  add catalogs_path, :changefreq => 'daily'

  Catalog.find_each do |catalog|
    add showcatalog_path(catalog.translit_url), :changefreq => 'daily', :lastmod => catalog.updated_at
  end

  Category.find_each do |category|
    add showcategory_path(category.translit_url), :changefreq => 'daily', :priority => 0.8, :lastmod => category.updated_at
  end

  Category.find_each do |category|
    category.twocategories.each do |twocategory|
      add showtwocategory_path(category.translit_url, twocategory.translit_url), :changefreq => 'daily', :priority => 0.8, :lastmod => twocategory.updated_at
    end
  end

  @posts = Post.where(visible: 1)
  @posts.find_each do |post|
    add showpost_path(post.translit_url), :changefreq => 'daily', :lastmod => post.updated_at
  end

  @companies = Company.where(moderation: 1)
  @companies.find_each do |company|
    add company_path(company), :changefreq => 'monthly', :lastmod => company.updated_at
  end
end
