class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.string :meta_title
      t.text :meta_desc
      t.text :meta_key

      t.timestamps
    end
  end
end
