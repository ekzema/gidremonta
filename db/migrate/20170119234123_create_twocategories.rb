class CreateTwocategories < ActiveRecord::Migration[5.0]
  def change
    create_table :twocategories do |t|
      t.string :name
      t.string :meta_title
      t.text :meta_desc
      t.text :meta_key
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
