class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string :name
      t.text :description
      t.text :body
      t.string :meta_title
      t.text :meta_desc
      t.text :meta_key
      t.integer :view_count, default: 0
      t.integer :like_count, default: 0
      t.references :twocategory, foreign_key: true

      t.timestamps
    end
  end
end
