class CreatePostSlideImages < ActiveRecord::Migration[5.0]
  def change
    create_table :post_slide_images do |t|
      t.references :post, foreign_key: true

      t.timestamps
    end
  end
end
