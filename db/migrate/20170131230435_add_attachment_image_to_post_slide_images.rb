class AddAttachmentImageToPostSlideImages < ActiveRecord::Migration
  def self.up
    change_table :post_slide_images do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :post_slide_images, :image
  end
end
