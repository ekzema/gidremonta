class CreateTwocomments < ActiveRecord::Migration[5.0]
  def change
    create_table :twocomments do |t|
      t.string :name
      t.text :body
      t.integer :moderation, default: 0
      t.string   :user_ip
      t.references :comment, foreign_key: true

      t.timestamps
    end
  end
end
