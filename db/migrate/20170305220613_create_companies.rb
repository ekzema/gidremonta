class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :site
      t.text :description
      t.string :state
      t.string :city
      t.integer :moderation, default: 0

      t.timestamps
    end
  end
end
