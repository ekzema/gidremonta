class AddColumnCatalogToCompanySkill < ActiveRecord::Migration[5.0]
  def change
    add_column :company_skills, :catalog_id, :integer
  end
end
