class AddMetaToCatalogs < ActiveRecord::Migration[5.0]
  def change
    add_column :catalogs, :meta_desc, :text
    add_column :catalogs, :meta_key, :text
  end
end
