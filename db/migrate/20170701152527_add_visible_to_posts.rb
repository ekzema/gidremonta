class AddVisibleToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :visible, :integer, default: 1
  end
end
