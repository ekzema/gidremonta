require 'test_helper'

class TwocommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @twocomment = twocomments(:one)
  end

  test "should get index" do
    get twocomments_url
    assert_response :success
  end

  test "should get new" do
    get new_twocomment_url
    assert_response :success
  end

  test "should create twocomment" do
    assert_difference('Twocomment.count') do
      post twocomments_url, params: { twocomment: { body: @twocomment.body, comment_id: @twocomment.comment_id, moderation: @twocomment.moderation, name: @twocomment.name } }
    end

    assert_redirected_to twocomment_url(Twocomment.last)
  end

  test "should show twocomment" do
    get twocomment_url(@twocomment)
    assert_response :success
  end

  test "should get edit" do
    get edit_twocomment_url(@twocomment)
    assert_response :success
  end

  test "should update twocomment" do
    patch twocomment_url(@twocomment), params: { twocomment: { body: @twocomment.body, comment_id: @twocomment.comment_id, moderation: @twocomment.moderation, name: @twocomment.name } }
    assert_redirected_to twocomment_url(@twocomment)
  end

  test "should destroy twocomment" do
    assert_difference('Twocomment.count', -1) do
      delete twocomment_url(@twocomment)
    end

    assert_redirected_to twocomments_url
  end
end
